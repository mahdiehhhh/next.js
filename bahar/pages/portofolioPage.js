import Navbar from '../components/Navbar';
import Title from '../components/Title';
import Image from 'next/image';
import Footer from '../components/Footer';
import FormInfo from '../components/FormInfo';
import Paginate from '../components/Paginate';
import 'react-tabs/style/react-tabs.css';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay } from 'swiper';
import 'swiper/css';
import { useState } from 'react';
import { useRouter } from 'next/router';
import Link from 'next/link';
import { AiOutlineLeft } from 'react-icons/ai';

const portofolioPage = () => {
  const all = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/sportapp.webp',
      alt: 'sportapp',
      title: 'Sport App',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/sportapp1.webp',
      alt: 'sportapp',
      title: 'Sport App',
    },
    {
      id: 3,
      image: '/assets/images/mainportfolio/sportapp2.webp',
      alt: 'sportapp',
      title: 'Sport App',
    },
    {
      id: 4,
      image: '/assets/images/mainportfolio/jobfinderapp.webp',
      alt: 'jobfinderapp',
      title: 'Job Finder App',
    },
    {
      id: 5,
      image: '/assets/images/mainportfolio/workoutprogramforatvbox.webp',
      alt: 'workoutprogramforatvbox',
      title: 'Workout Program For a TVbox',
    },
    {
      id: 6,
      image: '/assets/images/mainportfolio/scooterreservationapp.webp',
      alt: 'scooterreservationapp',
      title: 'Scooter Reservation App',
      name: 'scooter',
    },
    {
      id: 7,
      image: '/assets/images/mainportfolio/furniturestorewebsite.webp',
      alt: 'furniturestorewebsite',
      title: 'Furniture store website',
      name: 'furniture',
    },
    {
      id: 8,
      image: '/assets/images/mainportfolio/yaldalandingpage.webp',
      alt: 'yaldalandingpage',
      title: 'Yalda Landing Page',
      name: 'yalda',
    },
    {
      id: 9,
      image: '/assets/images/mainportfolio/sleepapneaapp.webp',
      alt: 'sleepapneaapp',
      title: 'Sleep apnea app',
    },
    {
      id: 10,
      image: '/assets/images/mainportfolio/fashionstoreapp.webp',
      alt: 'fashionstoreapp',
      title: 'Fashion store app',
      name: 'fashion',
    },
    {
      id: 11,
      image: '/assets/images/mainportfolio/metalproductsstoreapp.webp',
      alt: 'metalproductsstoreapp',
      title: 'Metal products store app',
    },
    {
      id: 12,
      image: '/assets/images/mainportfolio/lazyeyefixsoftware.webp',
      alt: 'lazyeyefixsoftware',
      title: 'Lazy eye fix software',
      name: 'eye',
    },
    {
      id: 13,
      image: '/assets/images/mainportfolio/Cartoncompanywebsite.webp',
      alt: 'Cartoncompanywebsite',
      title: 'Carton company website',
    },
    {
      id: 14,
      image: '/assets/images/mainportfolio/onlinepaymentgatewaywebsite.webp',
      alt: 'onlinepaymentgatewaywebsite',
      title: 'Online Payment Gateway website',
    },
    {
      id: 15,
      image: '/assets/images/mainportfolio/foamcompanywebsite.webp',
      alt: 'foamcompanywebsite',
      title: 'Foam company website',
    },
    {
      id: 16,
      image: '/assets/images/mainportfolio/galaxywebsite.webp',
      alt: 'galaxywebsite',
      title: 'Galaxy website',
    },
    {
      id: 17,
      image: '/assets/images/mainportfolio/music&moviewebsite.webp',
      alt: 'music&moviewebsite',
      title: 'Music & movie website',
    },
    {
      id: 18,
      image: '/assets/images/mainportfolio/medicalwebsite.webp',
      alt: 'medicalwebsite',
      title: 'Medical website',
    },
    {
      id: 19,
      image: '/assets/images/mainportfolio/programmingtraininglandingpage.webp',
      alt: 'programmingtraininglandingpage',
      title: 'Programming training landing page',
    },
    {
      id: 20,
      image: '/assets/images/mainportfolio/messengerlandingpage.webp',
      alt: 'messengerlandingpage',
      title: 'Messenger landing page',
    },
    {
      id: 21,
      image: '/assets/images/mainportfolio/followerstorewebsite.webp',
      alt: 'followerstorewebsite',
      title: 'Follower store website',
    },
    {
      id: 21,
      image: '/assets/images/mainportfolio/digitalmarketingwebsite.webp',
      alt: 'digitalmarketingwebsite',
      title: 'Digital marketing website',
      name: 'digital',
    },
    {
      id: 21,
      image: '/assets/images/mainportfolio/Storewebsite.webp',
      alt: 'Storewebsite',
      title: 'Store website',
    },
    {
      id: 22,
      image: '/assets/images/mainportfolio/Storewebsite.webp',
      alt: 'Storewebsite',
      title: 'Store website',
    },
    {
      id: 23,
      image: '/assets/images/mainportfolio/health/Healthwebsite.svg',
      alt: 'Healthwebsite.svg',
      title: 'Health Website',
    },
    {
      id: 24,
      image: '/assets/images/mainportfolio/Universitywebsite.svg',
      alt: 'Universitywebsite',
      title: 'University website',
    },
    {
      id: 25,
      image: '/assets/images/mainportfolio/Rentwebsite.svg',
      alt: 'Rentwebsite',
      title: 'Rent website',
    },
  ];

  const Shopping = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/furniturestorewebsite.webp',
      alt: 'furniturestorewebsite',
      title: 'Furniture store website',
      name: 'furniture',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/metalproductsstoreapp.webp',
      alt: 'metalproductsstoreapp',
      title: 'Metal products store app',
    },
    {
      id: 3,
      image: '/assets/images/mainportfolio/Storewebsite.webp',
      alt: 'Storewebsite',
      title: 'Store website',
    },
  ];

  const Educational = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/programmingtraininglandingpage.webp',
      alt: 'programmingtraininglandingpage',
      title: 'Programming training landing page',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/Universitywebsite.svg',
      alt: 'Universitywebsite',
      title: 'University website',
    },
  ];

  const software = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/sleepapneaapp.webp',
      alt: 'sleepapneaapp',
      title: 'Sleep apnea app',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/lazyeyefixsoftware.webp',
      alt: 'lazyeyefixsoftware',
      title: 'Lazy eye fix software',
      name: 'eye',
    },
    {
      id: 3,
      image: '/assets/images/mainportfolio/followerstorewebsite.webp',
      alt: 'followerstorewebsite',
      title: 'Follower store website',
    },
  ];

  const sport = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/sportapp.webp',
      alt: 'sportapp',
      title: 'Sport App',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/sportapp1.webp',
      alt: 'sportapp',
      title: 'Sport App',
    },
    {
      id: 3,
      image: '/assets/images/mainportfolio/sportapp2.webp',
      alt: 'sportapp',
      title: 'Sport App',
    },
    {
      id: 4,
      image: '/assets/images/mainportfolio/scooterreservationapp.webp',
      alt: 'scooterreservationapp',
      title: 'Scooter Reservation App',
      name: 'scooter',
    },
    {
      id: 5,
      image: '/assets/images/mainportfolio/workoutprogramforatvbox.webp',
      alt: 'workoutprogramforatvbox',
      title: 'Workout Program For a TVbox',
    },
  ];

  const Pay = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/onlinepaymentgatewaywebsite.webp',
      alt: 'onlinepaymentgatewaywebsite',
      title: 'Online Payment Gateway website',
    },
  ];

  const Digital = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/digitalmarketingwebsite.webp',
      alt: 'digitalmarketingwebsite',
      title: 'Digital marketing website',
      name: 'digital',
    },
  ];

  const Fashion = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/fashionstoreapp.webp',
      alt: 'fashionstoreapp',
      title: 'Fashion store app',
      name: 'fashion',
    },
  ];

  const Health = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/health/Healthwebsite.svg',
      alt: 'Healthwebsite.svg',
      title: 'Health Website',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/medicalwebsite.webp',
      alt: 'medicalwebsite',
      title: 'Medical website',
    },
  ];

  const other = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/jobfinderapp.webp',
      alt: 'jobfinderapp',
      title: 'Job Finder App',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/yaldalandingpage.webp',
      alt: 'yaldalandingpage',
      title: 'Yalda Landing Page',
      name: 'yalda',
    },
    {
      id: 3,
      image: '/assets/images/mainportfolio/Cartoncompanywebsite.webp',
      alt: 'Cartoncompanywebsite',
      title: 'Carton company website',
    },
    {
      id: 4,
      image: '/assets/images/mainportfolio/foamcompanywebsite.webp',
      alt: 'foamcompanywebsite',
      title: 'Foam company website',
    },
    {
      id: 5,
      image: '/assets/images/mainportfolio/galaxywebsite.webp',
      alt: 'galaxywebsite',
      title: 'Galaxy website',
    },
    {
      id: 6,
      image: '/assets/images/mainportfolio/music&moviewebsite.webp',
      alt: 'music&moviewebsite',
      title: 'Music & movie website',
    },
    {
      id: 7,
      image: '/assets/images/mainportfolio/messengerlandingpage.webp',
      alt: 'messengerlandingpage',
      title: 'Messenger landing page',
    },
    {
      id: 8,
      image: '/assets/images/mainportfolio/Rentwebsite.svg',
      alt: 'Rentwebsite',
      title: 'Rent website',
    },
  ];

  const tabs = [
    { title: 'All' },
    { title: 'Shopping' },
    { title: 'Educational' },
    { title: 'Software' },
    { title: 'sport' },
    { title: 'Pay' },
    { title: 'Digital marketing' },
    { title: 'Fashion store' },
    { title: 'Health' },
    { title: 'Other' },
  ];

  const [activeIndex, setActiveIndex] = useState(0);
  const handleClick = (index) => setActiveIndex(index);
  const checkActive = (index, className) =>
    activeIndex === index ? className : '';
  return (
    <>
      <div className='px-7 lg:px-16 pb-32 pt-10 portofoliopagebg'>
        <Link
          href='/'
          className='text-textcolor flex items-centers'>
          <span className='flex items-center flex-row-reverse'>
            Back To Home
            <AiOutlineLeft className='w-[20px]' />
          </span>
        </Link>
        <div className='flex justify-between mt-10'>
          <Title title='My portfolio' />

          <div>
            <Image
              src='/assets/images/star1.svg'
              alt='star1'
              width='0'
              height='0'
              sizes='100vw'
              style={{ width: '60px', height: 'auto', margin: 'auto' }}
              className='animate-float'
            />
            <Image
              src='/assets/images/star3.svg'
              alt='star'
              width='0'
              height='0'
              sizes='100vw'
              style={{
                width: '22px',
                height: 'auto',
                margin: 'auto',
                marginTop: '10px',
              }}
              className='animate-float'
            />
          </div>
        </div>

        <div className='tabs'>
          <Swiper
            className='mySwiperselect p-3 text-primary'
            slidesPerView={6}
            spaceBetween={10}
            // autoplay={{
            //   delay: 2500,
            //   disableOnInteraction: false,
            // }}
            breakpoints={{
              300: {
                slidesPerView: 2,
                spaceBetween: 20,
              },
              768: {
                slidesPerView: 4,
                spaceBetween: 40,
              },
              1024: {
                slidesPerView: 4,
                spaceBetween: 30,
              },
              1100: {
                slidesPerView: 6,
                spaceBetween: 20,
              },
            }}
            navigation={true}
            modules={[Autoplay]}>
            {tabs.map((item, index) => (
              <SwiperSlide key={index}>
                <button
                  className={`tab ${checkActive(index, 'active')}`}
                  onClick={() => handleClick(index)}>
                  {item.title}
                </button>
              </SwiperSlide>
            ))}
          </Swiper>
        </div>
        <div className='panels'>
          <div className={`panel ${checkActive(0, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={all}
            />
          </div>
          <div className={`panel ${checkActive(1, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={Shopping}
            />
          </div>
          <div className={`panel ${checkActive(2, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={Educational}
            />
          </div>
          <div className={`panel ${checkActive(3, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={software}
            />
          </div>
          <div className={`panel ${checkActive(4, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={sport}
            />
          </div>
          <div className={`panel ${checkActive(5, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={Pay}
            />
          </div>
          <div className={`panel ${checkActive(6, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={Digital}
            />
          </div>

          <div className={`panel ${checkActive(7, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={Fashion}
            />
          </div>
          <div className={`panel ${checkActive(8, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={Health}
            />
          </div>
          <div className={`panel ${checkActive(9, 'active')}`}>
            <Paginate
              itemsPerPage={9}
              items={other}
            />
          </div>
        </div>
      </div>

      <FormInfo />

      <Footer />
    </>
  );
};

export default portofolioPage;
