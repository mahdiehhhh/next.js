import Image from "next/image";

const Title = ({ title }) => {
  return (
    <div className="mb-6">
      <h2 className="text-titlecolor text-2xl md:text-3xl lg:text-3xl xl:text-3xl">
        {title}
      </h2>
      <Image
        src="/assets/images/underline.svg"
        alt="underLine"
        width="0"
        height="0"
        sizes="100vw"
        style={{ width: "150px", height: "auto", marginTop: "12px" }}
      />
    </div>
  );
};

export default Title;
