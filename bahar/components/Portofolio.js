import Image from 'next/image';
import Link from 'next/link';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Autoplay, Navigation } from 'swiper';
import Title from './Title';
import Modal from './Modal';
import 'swiper/css';
import 'swiper/css/navigation';
import { useState } from 'react';
const Portofolio = () => {
  const items = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/sportapp.webp',
      alt: 'sportapp',
      title: 'Sport App',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/sportapp1.webp',
      alt: 'storeapp',
      title: 'Store App',
    },
    {
      id: 3,
      image: '/assets/images/mainportfolio/sportapp2.webp',
      alt: 'educationalwebsite',
      title: 'Educational website',
    },
    {
      id: 4,
      image: '/assets/images/mainportfolio/jobfinderapp.webp',
      alt: 'jobfinderapp',
      title: 'Job Finder App',
    },
    {
      id: 5,
      image: '/assets/images/mainportfolio/workoutprogramforatvbox.webp',
      alt: 'workoutprogramforatvbox',
      title: 'Workout Program For a TVbox',
    },
    {
      id: 6,
      image: '/assets/images/mainportfolio/scooterreservationapp.webp',
      alt: 'scooterreservationapp',
      title: 'Scooter Reservation App',
      name: 'scooter',
    },
    {
      id: 7,
      image: '/assets/images/mainportfolio/furniturestorewebsite.webp',
      alt: 'furniturestorewebsite',
      title: 'Furniture store website',
      name: 'furniture',
    },
    {
      id: 8,
      image: '/assets/images/mainportfolio/yaldalandingpage.webp',
      alt: 'yaldalandingpage',
      title: 'Yalda Landing Page',
      name: 'yalda',
    },
    {
      id: 9,
      image: '/assets/images/mainportfolio/sleepapneaapp.webp',
      alt: 'sleepapneaapp',
      title: 'Sleep apnea app',
    },
    {
      id: 10,
      image: '/assets/images/mainportfolio/fashionstoreapp.webp',
      alt: 'fashionstoreapp',
      title: 'Fashion store app',
      name: 'fashion',
    },
    {
      id: 11,
      image: '/assets/images/mainportfolio/metalproductsstoreapp.webp',
      alt: 'metalproductsstoreapp',
      title: 'Metal products store app',
    },
    {
      id: 12,
      image: '/assets/images/mainportfolio/lazyeyefixsoftware.webp',
      alt: 'lazyeyefixsoftware',
      title: 'Lazy eye fix software',
      name: 'eye',
    },
    {
      id: 13,
      image: '/assets/images/mainportfolio/Cartoncompanywebsite.webp',
      alt: 'Cartoncompanywebsite',
      title: 'Carton company website',
    },
    {
      id: 14,
      image: '/assets/images/mainportfolio/onlinepaymentgatewaywebsite.webp',
      alt: 'onlinepaymentgatewaywebsite',
      title: 'Online Payment Gateway website',
    },
    {
      id: 15,
      image: '/assets/images/mainportfolio/foamcompanywebsite.webp',
      alt: 'foamcompanywebsite',
      title: 'Foam company website',
    },
    {
      id: 16,
      image: '/assets/images/mainportfolio/galaxywebsite.webp',
      alt: 'galaxywebsite',
      title: 'Galaxy website',
    },
    {
      id: 17,
      image: '/assets/images/mainportfolio/music&moviewebsite.webp',
      alt: 'music&moviewebsite',
      title: 'Music & movie website',
    },
    {
      id: 18,
      image: '/assets/images/mainportfolio/medicalwebsite.webp',
      alt: 'medicalwebsite',
      title: 'Medical website',
    },
    {
      id: 19,
      image: '/assets/images/mainportfolio/programmingtraininglandingpage.webp',
      alt: 'programmingtraininglandingpage',
      title: 'Programming training landing page',
    },
    {
      id: 20,
      image: '/assets/images/mainportfolio/messengerlandingpage.webp',
      alt: 'messengerlandingpage',
      title: 'Messenger landing page',
    },
    {
      id: 21,
      image: '/assets/images/mainportfolio/followerstorewebsite.webp',
      alt: 'followerstorewebsite',
      title: 'Follower store website',
    },
    {
      id: 22,
      image: '/assets/images/mainportfolio/digitalmarketingwebsite.webp',
      alt: 'digitalmarketingwebsite',
      title: 'Digital marketing website',
      name: 'digital',
    },
    {
      id: 23,
      image: '/assets/images/mainportfolio/Storewebsite.webp',
      alt: 'Storewebsite',
      title: 'Store website',
    },
    {
      id: 24,
      image: '/assets/images/mainportfolio/Universitywebsite.svg',
      alt: 'Universitywebsite',
      title: 'University website',
    },
    {
      id: 25,
      image: '/assets/images/mainportfolio/Healthwebsite.svg',
      alt: 'Healthwebsite',
      title: 'Health website',
    },
    {
      id: 26,
      image: '/assets/images/mainportfolio/Rentwebsite.svg',
      alt: 'Rentwebsite',
      title: 'Rent website',
    },
  ];

  const scooter = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/scooterslide/scooterslide1.webp',
      alt: 'scooterslide1',
      title: 'Scooter Reservation App',
      text: 'There is a scooter reservation app, but its function was a little different from the point of view that by booking a scooter, a person could do postal and tourism work, etc.',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/scooterslide/scooterslide2.webp',
      alt: 'scooterslide2',
    },
    {
      id: 3,
      image: '/assets/images/mainportfolio/scooterslide/scooterslide3.webp',
      alt: 'scooterslide3',
    },
    {
      id: 4,
      image: '/assets/images/mainportfolio/scooterslide/scooterslide4.webp',
      alt: 'scooterslide4',
    },
  ];

  const furniture = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/furniture/furnitureslide1.webp',
      alt: 'furnitureslide2',
      title: 'Furniture store website',
      text: 'A store site where different sellers can place their products.It was up to me to choose the color and I tried to find the best suitable color for this site.',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/furniture/furnitureslide2.webp',
      alt: 'furnitureslide2',
    },
  ];

  const yalda = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/yalda/yaldaslide1.webp',
      alt: 'yaldaslide2',
      title: 'Yalda Landing Page',
      text: 'I designed Yalda Landing and the challenge I had was that there was no site for ideas and I had to create it myself.',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/yalda/yaldaslide2.webp',
      alt: 'yaldaslide2',
    },
  ];

  const eye = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/eye/eyeslide1.webp',
      alt: 'eyeslide1',
      title: 'Lazy eye fix software',
      text: 'This was also a different task, the lazy eye software is installed on the system and you can check whether you have lazy eyes or not',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/eye/eyeslide2.webp',
      alt: 'eyeslide2',
    },
    {
      id: 3,
      image: '/assets/images/mainportfolio/eye/eyeslide3.webp',
      alt: 'eyeslide3',
    },
  ];

  const fashion = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/fashion/fashionslide1.webp',
      alt: 'fashionslide1',
      title: 'Digital marketing website',
      text: 'This is a fashion store concept for women',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/fashion/fashionslide2.webp',
      alt: 'fashionslide2',
    },
  ];

  const digital = [
    {
      id: 1,
      image: '/assets/images/mainportfolio/digital/digitalslide1.webp',
      alt: 'digitalslide1',
      title: 'Digital marketing website',
      text: 'It is a digital marketing site, the challenge was that I had to show programming languages and this method occupied little space and was more beautiful and different.',
    },
    {
      id: 2,
      image: '/assets/images/mainportfolio/digital/digitalslide2.webp',
      alt: 'digitalslide2',
    },
    {
      id: 3,
      image: '/assets/images/mainportfolio/digital/digitalslide3.webp',
      alt: 'digitalslide3',
    },
  ];

  const [showModal, setShowModal] = useState(false);
  const [data, setData] = useState(null);
  const modalHandler = (e) => {
    if (e.target.name) {
      setData(e.target.name);
      setShowModal(true);
    }
  };

  return (
    <div
      id='portfolio'
      className='lg:pl-5 xl:pl-14  mb-24'>
      <div className='grid grid-cols-6 lg:grid-cols-5 gap-8'>
        <div className='col-span-6 lg:col-span-2 xl:col-span-1 flex items-baseline justify-center pl-7 lg:mb-7 flex-col'>
          <Title title='My portfolio' />

          <p className='my-8 text-xl'>
            In my working years, I have worked on many projects and I have left
            a selection of them for you
          </p>

          <div className='flex items-start'>
            <div className='btn-holder'>
              <Link href='/portofolioPage'>
                <button className='btn-22'>
                  <span>View all</span>
                </button>
              </Link>
            </div>

            <Image
              src='/assets/images/arrow2.svg'
              alt='underLine'
              width='0'
              height='0'
              sizes='100vw'
              style={{ width: '100px', height: 'auto', marginTop: '-2rem' }}
            />
          </div>
        </div>
        <div className='col-span-6 lg:col-span-3 xl:col-span-4'>
          <Swiper
            className='mySwiper px-3 py-0 md:px-3 md:py-3 text-primary'
            slidesPerView={3}
            spaceBetween={30}
            // autoplay={{
            //   delay: 2500,
            //   disableOnInteraction: false,
            // }}
            breakpoints={{
              300: {
                slidesPerView: 1,
                spaceBetween: 20,
              },
              600: {
                slidesPerView: 2,
                spaceBetween: 40,
              },
              1024: {
                slidesPerView: 2,
                spaceBetween: 30,
              },
              1100: {
                slidesPerView: 3,
                spaceBetween: 30,
              },
            }}
            navigation={true}
            modules={[Autoplay, Navigation]}>
            {items.map((item, index) => (
              <>
                <SwiperSlide
                  key={item.id}
                  onClick={(e) => modalHandler(e)}>
                  <div className='slider__div rounded-3xl py-6 px-3 group '>
                    <div>
                      <Image
                        name={item.name}
                        src={item.image}
                        alt={item.alt}
                        width={310}
                        height={328}
                        className='rounded-2xl mx-auto'
                      />

                      {item.name && (
                        <Image
                          src='/assets/images/copy.svg'
                          alt={item.alt}
                          width='0'
                          height='0'
                          sizes='100vw'
                          style={{ width: '24px', height: '24px' }}
                          className='bg-portofoliocolor copy__image'
                        />
                      )}
                    </div>

                    <p
                      className='mx-4 text-textcolor  group-hover:visible  
                        ease-in duration-100 text-l
                      group-hover:ease-in group-hover:duration-100 slider__text-style'>
                      {item.title}
                    </p>
                  </div>
                </SwiperSlide>
              </>
            ))}
          </Swiper>

          {data && data === 'scooter' ? (
            <Modal
              showModal={showModal}
              setShowModal={setShowModal}
              itemsValue={scooter}
            />
          ) : data === 'furniture' ? (
            <Modal
              showModal={showModal}
              setShowModal={setShowModal}
              itemsValue={furniture}
            />
          ) : data === 'yalda' ? (
            <Modal
              showModal={showModal}
              setShowModal={setShowModal}
              itemsValue={yalda}
            />
          ) : data === 'eye' ? (
            <Modal
              showModal={showModal}
              setShowModal={setShowModal}
              itemsValue={eye}
            />
          ) : data === 'digital' ? (
            <Modal
              showModal={showModal}
              setShowModal={setShowModal}
              itemsValue={digital}
            />
          ) : data === 'fashion' ? (
            <Modal
              showModal={showModal}
              setShowModal={setShowModal}
              itemsValue={fashion}
            />
          ) : (
            ''
          )}
        </div>
      </div>
    </div>
  );
};

export default Portofolio;
