import Title from "./Title";
import Image from "next/image";

const Recognition = () => {
  const imageItems = [
    { image: "/assets/images/css.svg", alt: "css", title: "Css" },
    { image: "/assets/images/html.svg", alt: "htm", title: "Html" },
    {
      image: "/assets/images/analytic.svg",
      alt: "Analytics",
      title: "Analytics",
    },
  ];

  return (
    <div className='px-7 lg:px-16 mb-24'>
      <Title title='Relative recognition' />

      <div className='grid grid-cols-4 lg:grid-cols-6 gap-8 text-center'>
        {imageItems.map((item, index) => (
          <div
            className='col-span-1'
            key={index}>
            <Image
              src={item.image}
              alt={item.alt}
              width='0'
              height='0'
              sizes='100vw'
              style={{
                width: "100px",
                height: "auto",
              }}
              className='bg-portofoliocolor lg:rounded-3xl rounded-2xl mx-auto mb-3'
            />
            <span className='font-bold md:text-lg'>{item.title}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Recognition;
