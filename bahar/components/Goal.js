import Image from "next/image";

const Goal = () => {
  return (
    <>
      <div className=' md:px-0 flex items-center justify-center gap-10 lg:gap-40 lg:mx-16 bg-lightpurple py-10 lg:rounded-full mb-20'>
        <div>
          <Image
            src='/assets/images/meek1.svg'
            alt='meek'
            width='0'
            height='0'
            sizes='100vw'
            style={{ width: "60px", height: "auto", marginRight: "auto" }}
          />
          <p className='text-titlecolor lg:text-2xl font-bold mt-7'>meek</p>
        </div>

        <div>
          <Image
            src='/assets/images/support1.svg'
            alt='support'
            width='0'
            height='0'
            sizes='100vw'
            style={{ width: "60px", height: "auto", marginRight: "auto" }}
          />
          <p className='text-titlecolor lg:text-2xl font-bold mt-7'>Support</p>
        </div>

        <div>
          <Image
            src='/assets/images/ontime1.svg'
            alt='ontime'
            width='0'
            height='0'
            sizes='100vw'
            style={{ width: "60px", height: "auto", marginRight: "auto" }}
          />
          <p className='text-titlecolor lg:text-2xl font-bold mt-7'>On Time</p>
        </div>

        <div>
          <Image
            src='/assets/images/partial1.svg'
            alt='Partial'
            width='0'
            height='0'
            sizes='100vw'
            style={{ width: "60px", height: "auto", marginRight: "auto" }}
          />
          <p className='text-titlecolor lg:text-2xl font-bold mt-7'>Partial</p>
        </div>
      </div>
    </>
  );
};

export default Goal;
