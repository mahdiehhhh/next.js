/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,ts,jsx,tsx}",
    "./components/**/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    fontFamily: {
      montserrat: ["Montserrat"],
      caveat: ["Caveat"],
    },
    colors: {
      primary: "#FE8DF9",
      secondary: "#6F6F6F",
      titlecolor: "#383838",
      textcolor: "#434343",
      purple: "#876AD8",
      lightpurple: "#E1DAF5",
      portofoliocolor: "#D9D9D9",
      
    },
    extend: {
      keyframes: {
        float: {
          "0%, 100%": { opacity: 1 },
          "50%": { opacity: 0.3 },
        },
      },
      animation: {
        float: "float 3s ease-in-out infinite",
      },
    },
  },
  plugins: [],
};
