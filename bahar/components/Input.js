const Input = ({ name, type, formik, placeholder }) => {
  return (
    <div className="formControl">
      <input
        id={name}
        type={type}
        placeholder={placeholder}
        name="name"
        {...formik.getFieldProps(name)}
        className="w-full px-5 py-3 rounded-3xl appearance-none outline-none bg-[#ffff]"
      />

      {formik.errors[name] && formik.touched[name] && (
        <div className="text-primary absolute ">{formik.errors[name]}</div>
      )}
    </div>
  );
};

export default Input;
