import Image from 'next/image';
import React, { useEffect, useState } from 'react';
import ReactPaginate from 'react-paginate';

// Import Swiper styles
import 'swiper/css';
// import PortofolioSwiper from "./PortofolioSwiper";
function Items({ currentItems }) {
  // useEffect(() => {

  // }, []);
  return (
    <>
      <div className='grid grid-cols-3 mb-6'>
        {currentItems &&
          currentItems.map((item, index) => (
            <div className='col-span-4 lg:col-span-1 gap-2 bg-[#ffff] p-3 m-2 rounded-3xl portfoliopageStyle'>
              <Image
                src={item.image}
                alt={item.alt}
                width='0'
                height='0'
                sizes='100vw'
                style={{ width: '100%', height: 'auto' }}
                className=' rounded-2xl'
              />
            </div>
          ))}
      </div>
    </>
  );
}

function Paginate({ itemsPerPage, items }) {
  // Here we use item offsets; we could also use page offsets
  // following the API or data you're working with.
  const [itemOffset, setItemOffset] = useState(0);

  // Simulate fetching items from another resources.
  // (This could be items from props; or items loaded in a local state
  // from an API endpoint with useEffect and useState)
  const endOffset = itemOffset + itemsPerPage;
  const currentItems = items.slice(itemOffset, endOffset);
  const pageCount = Math.ceil(items.length / itemsPerPage);

  // Invoke when user click to request another page.
  const handlePageClick = (event) => {
    const newOffset = (event.selected * itemsPerPage) % items.length;
    window.scrollTo({ top: 0, behavior: 'smooth' });
    setItemOffset(newOffset);
  };

  return (
    <>
      <Items currentItems={currentItems} />

      <ReactPaginate
        breakLabel='...'
        nextLabel=' >'
        onPageChange={handlePageClick}
        pageRangeDisplayed={3}
        pageCount={pageCount}
        previousLabel='< '
        renderOnZeroPageCount={null}
        activeClassName='activeLink'
        containerClassName='pagination'
        pageLinkClassName='pageNum'
        previousLinkClassName='pageNum'
        nextLinkClassName='pageNum'
        pageClassName='pageNum'
      />
    </>
  );
}

export default Paginate;
