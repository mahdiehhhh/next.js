import Title from "./Title";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import { Autoplay } from "swiper";

const Industry = () => {
  const itemOne = [
    {
      title: "metal products store",
    },

    {
      title: "Grocery store",
    },

    {
      title: "Rental store",
    },

    {
      title: "sports",
    },

    {
      title: "Scooter reservation",
    },

    {
      title: "Yalda",
    },

    {
      title: "Digital marketing",
    },

    {
      title: "Fashion store",
    },
  ];

  const itemTwo = [
    { title: "Sleep apnea" },
    { title: "Music" },
    { title: "Fitness" },
    { title: "Pay" },
    { title: "Job finder" },
    { title: "Galaxy" },
    { title: "Furniture store" },
    { title: "Educational" },
    { title: "Doctor" },
    { title: "Corporate" },
    { title: "Programming Training" },
  ];

  return (
    <div className=' mb-24'>
      <div className='px-7 lg:px-16'>
        <Title title='The industries I worked in' />
      </div>

      <div>
        <Swiper
          className='mySwiper4'
          slidesPerView={20}
          spaceBetween={30}
          autoplay={{
            delay: 2500,
            disableOnInteraction: false,
          }}
          pagination={{
            clickable: true,
          }}
          breakpoints={{
            300: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 40,
            },
            800: {
              slidesPerView: 4,
              spaceBetween: 0,
            },
            1024: {
              slidesPerView: 7,
              spaceBetween: 0,
            },
          }}
          modules={[Autoplay]}>
          {itemOne.map((item, index) => (
            <SwiperSlide key={index}>
              <p className='text-secondary bg-lightpurple font-bold py-3 px-7 rounded-3xl text-center w-fit mx-auto'>
                {item.title}
              </p>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>

      <div>
        <Swiper
          className='mySwiper5'
          slidesPerView={20}
          spaceBetween={30}
          autoplay={{
            delay: 2800,
            disableOnInteraction: false,
          }}
          pagination={{
            clickable: true,
          }}
          breakpoints={{
            300: {
              slidesPerView: 2,
              spaceBetween: 20,
            },
            768: {
              slidesPerView: 2,
              spaceBetween: 40,
            },
            800: {
              slidesPerView: 4,
              spaceBetween: 0,
            },
            1024: {
              slidesPerView: 9,
              spaceBetween: 0,
            },
          }}
          modules={[Autoplay]}>
          {itemTwo.map((item, index) => (
            <SwiperSlide key={index}>
              <p className='text-secondary bg-lightpurple py-3 px-7 rounded-3xl font-bold text-center w-fit mx-auto'>
                {item.title}
              </p>
            </SwiperSlide>
          ))}
        </Swiper>
      </div>
    </div>
  );
};

export default Industry;
