import Title from "./Title";
import Image from "next/image";

const Process = () => {
  return (
    <div
      id='workprocess'
      className='px-7 lg:px-16 mb-24'>
      <Title title='My work process' />
      <div className='grid grid-row lg:grid-cols-6'>
        <div className='col-span-3 flex items-center order-last lg:order-first'>
          <p className='md:text-[20px] text-[18] text-secondary leading-[40px]'>
            The first step is to have a meeting so that I can get to know your
            work process and the goals and direction of the company. To move
            forward with a better vision. The second stage is to research and
            study competitors and collect information for the product that is
            going to be designed.
            <br /> The third step is to design the wireframe or the initial
            design and send it to the customer, it is edited if needed, and
            after approval, it enters the next step. The fourth step is to
            design the main design and responsive versions and deliver them to
            the programmer. <br />
            The fifth step, after the implementation, all the pages are checked
            so that they are similar to the design and delivered to the
            customer.
            <br /> The sixth step, if necessary, all kinds of usability tests
            are performed on your product.
          </p>
        </div>
        <div className='col-span-3 mb-5 md:mb-0'>
          <Image
            src='/assets/images/workImage.svg'
            alt='image'
            width='0'
            height='0'
            sizes='100vw'
            style={{ width: "80%", height: "auto" }}
            className='mx-auto'
          />
        </div>
      </div>
    </div>
  );
};

export default Process;
