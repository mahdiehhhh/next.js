import Image from "next/image";
import Title from "./Title";

const Professions = () => {
  const imageItems = [
    { image: "/assets/images/figma.svg", alt: "figma", title: "Figma" },
    { image: "/assets/images/Miro.svg", alt: "Miro", title: "Miro" },
    { image: "/assets/images/hotjar.svg", alt: "hotjar", title: "Hotjar" },
    { image: "/assets/images/xd.svg", alt: "xd", title: "Xd" },
    {
      image: "/assets/images/photoshop.svg",
      alt: "photoshop",
      title: "Photoshop",
    },
    {
      image: "/assets/images/illustration.svg",
      alt: "illustration",
      title: "Illustrator",
    },
  ];

  return (
    <div
      className='px-7 lg:px-16 mb-24'
      id='skills'>
      <div className='flex justify-between'>
        <Title title='My professions' />
        <div className='flex justify-end items-baseline'>
          <Image
            src='/assets/images/star3.svg'
            alt='star3'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: "22px",
              height: "auto",
            }}
            className='mx-auto animate-float'
          />
          <Image
            src='/assets/images/star1.svg'
            alt='star1'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: "60px",
              height: "auto",
            }}
            className='ml-5 animate-float'
          />
          <Image
            src='/assets/images/star3.svg'
            alt='star4'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: "22px",
              height: "auto",
            }}
            className='mx-auto animate-float'
          />
        </div>
      </div>

      <div className='grid grid-cols-4  lg:grid-cols-6 gap-8 text-center'>
        {imageItems.map((item, index) => (
          <div
            className='col-span-1'
            key={index}>
            <Image
              src={item.image}
              alt={item.alt}
              width='0'
              height='0'
              sizes='100vw'
              style={{
                width: "100px",
                height: "auto",
              }}
              className='bg-portofoliocolor  lg:rounded-3xl rounded-2xl mx-auto mb-3'
            />
            <span className='font-bold text-sm md:text-lg'>{item.title}</span>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Professions;
