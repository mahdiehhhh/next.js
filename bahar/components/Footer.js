import Link from 'next/link';
import { useState } from 'react';

import {
  AiOutlineInstagram,
  AiOutlineWhatsApp,
  AiOutlineLinkedin,
} from 'react-icons/ai';

const Footer = () => {
  const [show, setShow] = useState(false);
  return (
    <div className='grid grid-cols-6 pb-8  md:pb-10  lg:px-16'>
      <div className='lg:col-span-5 col-span-6'>
        <p className='font-bold text-secondary text-center text-xl lg:ml-64'>
          Designed by Bahar Davoudi
        </p>
      </div>
      <div className='lg:col-span-1 col-span-6 mt-5 lg:mt-0'>
        <div className='flex justify-center items-center gap-4'>
          <Link
            href='https://telegram.me/bahar_davoudi76'
            className='bg-[#ffff] w-[35px] h-[35px] rounded-full flex items-center justify-center hover:bg-primary duration-300 ease-in-out'
            onMouseEnter={() => setShow(true)}
            onMouseLeave={() => setShow(false)}>
            {!show && (
              <img
                src='/assets/images/telegram.svg'
                className='w-[18px]'
              />
            )}

            {show && (
              <img
                src='/assets/images/telegramwhite.svg'
                className='w-[18px]'
              />
            )}
          </Link>
          <Link href='http://instagram.com/bahar_davoudi76'>
            <AiOutlineInstagram
              size={35}
              className='bg-[#FFFFFF] p-2 text-primary rounded-full hover:text-[#ffff] hover:bg-primary duration-300 ease-in-out'
            />
          </Link>
          <Link href='https://www.linkedin.com/in/bahareh-davoudi-115b87172'>
            <AiOutlineLinkedin
              size={35}
              className='bg-[#FFFFFF] p-2 text-primary rounded-full hover:text-[#ffff] hover:bg-primary duration-300 ease-in-out'
            />
          </Link>
        </div>
      </div>
    </div>
  );
};

export default Footer;
