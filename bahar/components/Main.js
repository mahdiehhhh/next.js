import Image from 'next/image';
import Link from 'next/link';
import { useState } from 'react';

import { AiOutlineInstagram, AiOutlineLinkedin } from 'react-icons/ai';

const Main = () => {
  const [show, setShow] = useState(false);

  return (
    <div
      id='main'
      className='pt-5 md:pt-20 px-7 lg:px-16'>
      <div className='grid grid-cols-1 lg:grid lg:grid-cols-5  gap-4  pt-10'>
        <div className='col-span-2 mb-6 lg:mb-0'>
          <div className='lg:hidden'>
            <Image
              src='/assets/images/star1.svg'
              alt='Bahar'
              width='0'
              height='0'
              sizes='100vw'
              style={{ width: '60px', height: 'auto', marginRight: 'auto' }}
              className='animate-float'
            />

            <Image
              src='/assets/images/star2.svg'
              alt='Bahar'
              width='0'
              height='0'
              sizes='100vw'
              style={{ width: '28px', height: 'auto', marginRight: 'auto' }}
              className='animate-float'
            />
          </div>

          <Image
            src='/assets/images/main.svg'
            alt='Bahar'
            width='0'
            height='0'
            sizes='100vw'
            style={{ width: '450px', height: 'auto', margin: 'auto' }}
          />

          {/* <div className="lg:hidden mt-7 lg:mt-0">
            <Image
              src="/assets/images/star1.svg"
              alt="Bahar"
              width="0"
              height="0"
              sizes="100vw"
              style={{ width: "60px", height: "auto", marginLeft: "auto" }}
              className="animate-float"
            />

            <Image
              src="/assets/images/star3.svg"
              alt="Bahar"
              width="0"
              height="0"
              sizes="100vw"
              style={{ width: "22px", height: "auto", marginLeft: "auto" }}
              className="animate-float"
            />
          </div> */}
        </div>
        <div className='col-span-3 flex items-baseline justify-end flex-col'>
          <h1 className='text-center w-full md:text-3xl  lg:text-6xl  lg:text-left font-bold text-titlecolor mb-4'>
            Bahar Davoudi
          </h1>

          <p className='text-center w-full text-purple md:text-4xl text-2xl lg:text-left font-bold mb-5 '>
            UI / UX Designer
          </p>

          <div className='lg:w-[450px] mb-10 text-center lg:text-left'>
            <p className='leading-9 text-textcolor '>
              I am Bahare Davoudi, born in March 1998, I am a graduate of
              software engineering. I have been working in the field of UIUX for
              5 years. Here I share my portfolio with you.
            </p>
          </div>

          <div className='flex lg:justify-between justify-center w-full mb-8'>
            <div className='btn-holder'>
              <a
                href='/assets/resume/Portfolio.pdf'
                download
                className='h-20'>
                {/* <button className="btn btn-1 hover-filled-slide-right ">
                  <span>Download my resume</span>
                </button> */}
                <button className='btn-22'>
                  <span>Download my resume</span>
                </button>
              </a>
            </div>

            <div className='hidden lg:block'>
              <Image
                src='/assets/images/star1.svg'
                alt='star1'
                width='0'
                height='0'
                sizes='100vw'
                style={{ width: '60px', height: 'auto', margin: 'auto' }}
                className='animate-float'
              />
              <Image
                src='/assets/images/star3.svg'
                alt='star'
                width='0'
                height='0'
                sizes='100vw'
                style={{
                  width: '22px',
                  height: 'auto',
                  margin: 'auto',
                  marginTop: '10px',
                }}
                className='animate-float'
              />
            </div>
          </div>
        </div>
      </div>

      <div className='grid grid-cols-5 gap-4 style__section-one'>
        <div className='col-span-2 hidden lg:block'>
          <Image
            src='/assets/images/arrow1.svg'
            alt='Bahar'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: '150px',
              height: 'auto',
              marginLeft: 'auto',
              marginTop: '-1rem',
            }}
          />
        </div>

        <div className='col-span-6 lg:col-span-2 flex items-center justify-center lg:justify-start mt-3 lg:gap-4'>
          <span className='text-secondary font-bold'>Check Out My</span>

          <Image
            src='/assets/images/Line.png'
            alt='Bahar'
            width='0'
            height='0'
            style={{
              width: '30px',
              height: 'auto',
              marginRight: '25px',
              marginLeft: '25px',
            }}
          />

          <div className='flex justify-center items-center gap-2 md:gap-4'>
            <a href='https://www.linkedin.com/in/bahareh-davoudi-115b87172'>
              <AiOutlineLinkedin
                size={35}
                className='bg-[#FFFFFF] p-2  text-primary rounded-full hover:text-[#ffff] hover:bg-primary duration-300 ease-in-out'
              />
            </a>
            <Link href='http://instagram.com/bahar_davoudi76'>
              <AiOutlineInstagram
                size={35}
                className='bg-[#FFFFFF] p-2  text-primary rounded-full hover:text-[#ffff] hover:bg-primary duration-300 ease-in-out'
              />
            </Link>
            <Link
              href='https://telegram.me/bahar_davoudi76'
              className='bg-[#ffff] w-[35px] h-[35px] rounded-full flex items-center justify-center hover:bg-primary duration-300 ease-in-out'
              onMouseEnter={() => setShow(true)}
              onMouseLeave={() => setShow(false)}>
              {!show && (
                <img
                  src='/assets/images/telegram.svg'
                  className='w-[18px]'
                />
              )}

              {show && (
                <img
                  src='/assets/images/telegramwhite.svg'
                  className='w-[18px]'
                />
              )}
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Main;
