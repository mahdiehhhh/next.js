import Title from './Title';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import { Autoplay, Pagination } from 'swiper';
import 'swiper/css/pagination';
import Image from 'next/image';
import AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';

const Comments = () => {
  const commentsItem = [
    {
      comment: 'Super manager with a plan',
      name: 'Keivan engineer ',
      image: '/assets/images/Keivan.svg',
      alt: 'ellips',
      companyName: 'Serkan digital marketing manager',
      audio: '/assets/audio/AUD-20230227-WA0006 (1).m4a',
    },
    {
      comment: 'I am happy that I had several projects with them',
      name: 'Shadman engineer',
      image: '/assets/images/shadman.svg',
      alt: 'ellips',
      companyName: 'SEO and entrepreneur',
      audio: '/assets/audio/msg115227174-113869.ogg',
    },
    {
      comment: 'I enjoy working with them',
      name: 'Alizadeh engineer',
      image: '/assets/images/alizadeh.jpg',
      alt: 'ellips',
      companyName: 'Ronia soft manager',
      audio: '/assets/audio/Voice 002 (1).m4a',
    },
    {
      comment:
        "I'm very happy to work with Mrs. Davoudi. our project was a heavy university project and she delivered the project with attention to detail and according to the time announced",
      name: 'Mr. Jai Bashi',
      image: '/assets/images/Jai.jpg',
      alt: 'ellips',
      companyName: 'Deputy General of the University',
    },
    {
      comment:
        'I wanted to thank you for your time and patience for my site.  I was really satisfied with the result and it was what I wanted Thank you to your professional team.',
      name: 'Sahar davoudi',
      image: '/assets/images/sahar.svg',
      alt: 'ellips',
      companyName: 'Accountants _ manager',
    },
  ];

  return (
    <div
      id='client'
      className='px-7 lg:px-16 mb-24'>
      <div className='grid grid-cols-8'>
        <div className='col-span-1 hidden md:flex justify-end items-center'>
          <div className='flex justify-end items-center'>
            <Image
              src='/assets/images/star3.svg'
              alt='star3'
              width='0'
              height='0'
              sizes='100vw'
              style={{
                width: '22px',
                height: 'auto',
              }}
              className='mx-auto animate-float'
            />
            <Image
              src='/assets/images/star1.svg'
              alt='star1'
              width='0'
              height='0'
              sizes='100vw'
              style={{
                width: '60px',
                height: 'auto',
              }}
              className='ml-5 animate-float'
            />
            <Image
              src='/assets/images/star3.svg'
              alt='star4'
              width='0'
              height='0'
              sizes='100vw'
              style={{
                width: '22px',
                height: 'auto',
              }}
              className='mx-auto animate-float'
            />
          </div>
        </div>

        <div className='col-span-12 md:col-span-6'>
          <h1 className='font-caveat my-16 md:my-24 text-center lg:text-6xl '>
            The goal is a planned dream :)
          </h1>
        </div>
      </div>
      <Title title='Whats client say About me' />
      <Swiper
        className='mySwiper3 p-3 text-primary'
        slidesPerView={5}
        spaceBetween={30}
        // centeredSlides={true}
        // autoplay={{
        //   delay: 2500,
        //   disableOnInteraction: false,
        // }}
        pagination={{
          clickable: true,
        }}
        breakpoints={{
          300: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          600: {
            slidesPerView: 2,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 4,
            spaceBetween: 30,
          },
        }}
        modules={[Autoplay, Pagination]}>
        {commentsItem.map((item, index) => (
          <SwiperSlide key={index}>
            <div className='swiper3-slide h-full flex flex-col justify-between bg-[#ffff]  rounded-3xl p-3 bgStyle'>
              <Image
                src={item.image}
                alt={item.alt}
                width='0'
                height='0'
                sizes='100vw'
                style={{ width: '70px', height: '70px' }}
                className='absolute right-[-1rem] top-[-2rem] border-4 border-[#ffff] rounded-full'
              />
              <p className='text-secondary text-sm my-7 leading-7'>
                {item.comment}
              </p>
              {item.audio && (
                <div className='audioSection'>
                  <AudioPlayer
                    autoPlay={false}
                    src={item.audio}
                    showJumpControls={false}
                    autoPlayAfterSrcChange={false}
                  />
                </div>
              )}
              <div>
                <p className='text-textcolor mb-2'>{item.name}</p>
                <p className='text-secondary'>{item.companyName}</p>
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default Comments;
