import Image from 'next/image';
import Link from 'next/link';
import { useRouter } from 'next/router';
import React, { useState, useEffect } from 'react';
import { AiOutlineClose } from 'react-icons/ai';
const Navbar = () => {
  const [nav, setNav] = useState(false);
  const [shadow, setShadow] = useState(false);

  const router = useRouter();
  const currentRoute = router.asPath;

  const navHandler = () => {
    setNav(!nav);
  };

  useEffect(() => {
    const handleShadow = () => {
      if (window.scrollY >= 90) {
        setShadow(true);
      } else {
        setShadow(false);
      }
    };
    window.addEventListener('scroll', handleShadow);
  }, []);

  return (
    <div
      className={
        shadow
          ? 'flex w-full h-10 lg:h-20 shadow-sm z-[100] fixed bg-[#fafafb]'
          : 'flex w-full h-10 lg:h-20 z-[100] fixed'
      }>
      <div className=' w-full h-full '>
        <div className='grid grid-cols-5 h-full gap-4'>
          <div className='col-span-1'>
            <div className={shadow ? ' hidden' : 'hidden lg:block'}>
              <Image
                src='/assets/images/star2.svg'
                alt='star'
                width='0'
                height='0'
                sizes='100vw'
                style={{
                  width: '28px',
                  height: 'auto',
                  margin: 'auto',
                  marginTop: '10px',
                }}
                className='animate-float'
              />

              <Image
                src='/assets/images/star4.svg'
                alt='star'
                width='0'
                height='0'
                sizes='100vw'
                style={{
                  width: '18px',
                  height: 'auto',
                  margin: 'auto',
                  marginTop: '10px',
                }}
                className='animate-float'
              />
            </div>
          </div>
          <div className='col-span-3 hidden lg:flex items-center mx-auto'>
            <ul className='flex items-center'>
              <Link
                href='#main'
                scroll={false}
                className={
                  currentRoute === '/'
                    ? 'active'
                    : currentRoute === '/#main'
                    ? 'active'
                    : 'nonActive'
                }>
                <li>Home</li>
              </Link>

              <Link
                href='#client'
                scroll={false}
                className={
                  currentRoute === '/#client' ? 'active' : 'nonActive'
                }>
                <li>Client</li>
              </Link>

              <Link
                href='#workprocess'
                scroll={false}
                className={
                  currentRoute === '/#workprocess' ? 'active' : 'nonActive'
                }>
                <li>Work process</li>
              </Link>

              <Link
                href='#portfolio'
                scroll={false}
                className={
                  currentRoute === '/#portfolio' ? 'active' : 'nonActive'
                }>
                <li>My portfolio</li>
              </Link>

              <Link
                href='#skills'
                scroll={false}
                className={
                  currentRoute === '/#skills' ? 'active' : 'nonActive'
                }>
                <li>Skills</li>
              </Link>

              <Link
                href='#contact'
                scroll={false}
                className={
                  currentRoute === '/#contact' ? 'active' : 'nonActive'
                }>
                <li>Contact</li>
              </Link>
            </ul>
          </div>
        </div>
      </div>

      <div
        onClick={navHandler}
        className='flex items-center px-3 lg:hidden'>
        <Image
          src='/assets/images/menu.svg'
          alt='menu'
          width='0'
          height='0'
          sizes='100vw'
          style={{
            width: '30px',
            height: 'auto',
            margin: 'auto',
            marginTop: '10px',
          }}
        />
      </div>

      <div
        className={
          nav
            ? 'lg:hidden fixed left-0 top-0 bg-black/60 w-full h-screen ease-in duration-300 bg-[#6633998f]'
            : 'ease-in duration-300 w-[-100%]'
        }>
        <div
          className={
            nav
              ? 'lg:hidden fixed left-0 top-0 w-52  h-screen p-3 ease-in duration-500 bg-[#ffff]'
              : 'fixed left-[-100%] h-screen ease-in duration-500 '
          }>
          <div
            onClick={navHandler}
            className='flex w-full justify-end'>
            <span className='rounded-full shadow-md p-2'>
              <AiOutlineClose
                size={20}
                className='text-[#6F6F6F]'
              />
            </span>
          </div>

          <div>
            <ul className='mt-10'>
              <Link
                href='#main'
                scroll={false}
                className={
                  currentRoute === '/'
                    ? 'active'
                    : currentRoute === '/#main'
                    ? 'active'
                    : 'nonActive'
                }>
                <li className='mb-3'>Home</li>
              </Link>

              <Link
                href='#client'
                scroll={false}
                className={
                  currentRoute === '/#client' ? 'active' : 'nonActive'
                }>
                <li>Client</li>
              </Link>

              <Link
                href='#workprocess'
                scroll={false}
                className={
                  currentRoute === '/#workprocess' ? 'active' : 'nonActive'
                }>
                <li>Work process</li>
              </Link>

              <Link
                href='#portfolio'
                scroll={false}
                className={
                  currentRoute === '/#portfolio' ? 'active' : 'nonActive'
                }>
                <li>My portfolio</li>
              </Link>

              <Link
                href='#skills'
                scroll={false}
                className={
                  currentRoute === '/#skills' ? 'active' : 'nonActive'
                }>
                <li>Skills</li>
              </Link>

              <Link
                href='#contact'
                scroll={false}
                className={
                  currentRoute === '/#contact' ? 'active' : 'nonActive'
                }>
                <li>Contact</li>
              </Link>
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Navbar;
