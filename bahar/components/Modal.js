import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay, Navigation } from "swiper";

import { AiOutlineCloseCircle } from "react-icons/ai";

import Image from "next/image";
const Modal = ({ showModal, setShowModal, itemsValue }) => {
  console.log(itemsValue, "itemsValue");
  return (
    <>
      {showModal ? (
        <>
          <div className='justify-center items-top md:items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none bg-[#0000007a] pt-20'>
            <div className='relative w-auto my-6 mx-auto max-w-[23rem] md:max-w-2xl'>
              {/*content*/}
              <div className='border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-white outline-none focus:outline-none  bg-[#fff]'>
                {/*header*/}

                <div className='flex items-start justify-between pt-5 px-5'>
                  <button
                    className=' ease-linear transition-all duration-150'
                    type='button'
                    onClick={() => setShowModal(false)}>
                    <AiOutlineCloseCircle
                      size={30}
                      className=' text-[#eb5757] '
                    />
                  </button>
                </div>

                <div className='relative px-3 flex-auto'>
                  <Swiper
                    className='mySwiperModal'
                    slidesPerView={1}
                    spaceBetween={30}
                    breakpoints={{
                      300: {
                        slidesPerView: 1,
                        spaceBetween: 20,
                      },
                      768: {
                        slidesPerView: 1,
                        spaceBetween: 40,
                      },
                      1024: {
                        slidesPerView: 1,
                        spaceBetween: 30,
                      },
                    }}
                    navigation={true}
                    modules={[Autoplay, Navigation]}>
                    {itemsValue.map((item, index) => (
                      <SwiperSlide
                        key={index}
                        onClick={() => setShowModal(true)}>
                        <div className=' rounded-3xl p-0  group '>
                          <Image
                            src={item.image}
                            alt={item.alt}
                            width='0'
                            height='0'
                            sizes='100vw'
                            style={{ width: "100%", height: "100%" }}
                            className=' rounded-2xl mx-auto'
                          />
                        </div>
                      </SwiperSlide>
                    ))}
                  </Swiper>

                  {itemsValue.map((item, index) => (
                    <div
                      className='pb-5 px-10'
                      key={index}>
                      <h3>{item.title}</h3>
                      <p>{item.text}</p>
                    </div>
                  ))}
                </div>
                {/*footer*/}
              </div>
            </div>
          </div>
          <div className='opacity-25 fixed inset-0 z-40 bg-black'></div>
        </>
      ) : null}
    </>
  );
};

export default Modal;
