import { Swiper, SwiperSlide } from "swiper/react";
import { Autoplay } from "swiper";
import Title from "./Title";
import Image from "next/image";
import "swiper/css";

const History = () => {
  const portofilioItems = [
    {
      text: "Currently, I am a small member of the big Mapna family",
      date: "Nov 2022",
      title: "Mapna(MECO) Company",
      image: "/assets/images/ellips.svg",
      alt: "Mapna",
    },
    {
      text: "I worked as a project and remote work designer UI/UX",
      date: "Nov 2021  |  Nov 2022",
      title: "NikAmooz Company",
      image: "/assets/images/ellips.svg",
      alt: "NikAmooz",
    },
    {
      text: "During the Corona days, I was a freelancer like many others",
      date: "Mar 2020",
      title: "Freelancer",
      image: "/assets/images/ellips.svg",
      alt: "Freelance",
    },
    {
      text: "As a designer ui/ux",
      date: "Oct 2019 |  Mar 2020",
      title: "Sepenta e-commerce company ",
      image: "/assets/images/ellips.svg",
      alt: "Sepanta",
    },
    {
      text: "We were a group of 8 people in the employment school of Azad Shahr Quds University, where I worked with the guys as a designer ui/ux",
      date: "May 2018 | Mar 2020",
      title: "Employment school",
      image: "/assets/images/ellips.svg",
      alt: "ellips",
    },
  ];

  return (
    <div className='pl-7 pr-7 lg:pl-16 lg:pr-0 mb-24'>
      <Title title='My work history' />

      <Swiper
        className='mySwiper2 p-3 text-primary'
        slidesPerView={5}
        spaceBetween={30}
        centeredSlides={false}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        breakpoints={{
          300: {
            slidesPerView: 1,
            spaceBetween: 10,
            centeredSlides: false,
          },

          600: {
            slidesPerView: 2,
            spaceBetween: 40,
          },

          800: {
            slidesPerView: 3,
            spaceBetween: 30,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 30,
          },

          1100: {
            slidesPerView: 4,
            spaceBetween: 30,
          },

          1473: {
            slidesPerView: 5,
            spaceBetween: 30,
          },
        }}
        modules={[Autoplay]}>
        {portofilioItems.map((item, index) => (
          <SwiperSlide
            key={index}
            content={item}>
            <div className='history__swiper flex items-center justify-center rounded-3xl p-3 active:ml-1'>
              <div className='rotate-[270deg] w-f '>
                <h4 className='text-textcolor '>{item.title}</h4>
              </div>
              <div
                className='group overlay w-12 h-20 bg-purple rounded-r-full  
              absolute 
               hover:rounded-3xl ease-in duration-120 cursor-pointer p-4 media__style'>
                <div
                  className='opacity-0 group-hover:opacity-100  group-hover:ease-in 
                group-hover:duration-1000 overlay__div'>
                  <div className='flex items-center'>
                    <Image
                      src={item.image}
                      alt={item.alt}
                      width='0'
                      height='0'
                      sizes='100vw'
                      style={{ width: "40px", height: "auto" }}
                    />
                    <h4 className='text-[#ffff]'>{item.title}</h4>
                  </div>

                  <div className='mt-5 ml-5'>
                    <span className='text-[#ffff] text-sm font-thin'>
                      {item.date}
                    </span>
                    <p className='text-[#ffff] mt-8 text-sm'>{item.text}</p>
                  </div>
                </div>
              </div>
            </div>
          </SwiperSlide>
        ))}
      </Swiper>

      <div className='grid grid-cols-8'>
        <div className='col-span-1'>
          <div className='flex justify-end items-baseline'>
            <Image
              src='/assets/images/star3.svg'
              alt='star3'
              width='0'
              height='0'
              sizes='100vw'
              style={{
                width: "22px",
                height: "auto",
              }}
              className='mx-auto animate-float'
            />
            <Image
              src='/assets/images/star1.svg'
              alt='star1'
              width='0'
              height='0'
              sizes='100vw'
              style={{
                width: "60px",
                height: "auto",
              }}
              className='ml-5 animate-float'
            />
            <Image
              src='/assets/images/star3.svg'
              alt='star4'
              width='0'
              height='0'
              sizes='100vw'
              style={{
                width: "22px",
                height: "auto",
              }}
              className='mx-auto animate-float'
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default History;
