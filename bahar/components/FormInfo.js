import Image from 'next/image';
import Input from '../components/Input';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import axios from 'axios';

const Form = () => {
  const initialValues = { phone: '', name: '', email: '' };

  const validationSchema = Yup.object({
    phone: Yup.string().required('Phone number is required'),
    name: Yup.string().required('Name is required'),
    email: Yup.string().email('Invalid email').required('Email is required'),
  });

  // const onSubmit = async (values) => {
  //   try {
  //     const response = await axios.post('/api/send-email', values);

  //     if (response.status === 200) {
  //       toast.success('Email sent successfully!');
  //     } else {
  //       toast.error('Failed to send email.');
  //     }
  //   } catch (error) {
  //     console.error(error);
  //     toast.error('An error occurred while sending the email.');
  //   }
  // };

  const onSubmit = async (values) => {
    try {
      const response = await axios.post('/api/send-email', values);

      if (response.status === 200) {
        toast.success('Email sent successfully!');
      } else {
        toast.error('Failed to send email.');
      }
    } catch (error) {
      console.error('Error:', error);
      toast.error('An error occurred while sending the email.');
    }
  };

  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    validateOnMount: true,
    enableReinitialize: true,
  });

  return (
    <div
      id='contact'
      className='mb-8 lg:mx-16 md:p-7 md:mb-16 lg:rounded-3xl bg-purple py-7 lg:p-10'>
      <ToastContainer />
      <div className='grid xl:grid-cols-6 lg:grid-cols-8 '>
        <div className='col-span-1 lg:flex items-end justify-center hidden'>
          <Image
            src='/assets/images/star2.svg'
            alt='star3'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: '28px',
              height: 'auto',
            }}
            className='mx-auto'
          />
          <Image
            src='/assets/images/star1.svg'
            alt='star1'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: '60px',
              height: 'auto',
            }}
            className='ml-5'
          />
          <Image
            src='/assets/images/star4.svg'
            alt='star4'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: '18px',
              height: 'auto',
            }}
            className='mx-auto'
          />
        </div>
        <div className='lg:col-span-6 xl:col-span-4 col-span-6'>
          <p className='text-[#FFFFFF] text-xl leading-9 lg:px-16 mb-8 text-center'>
            If you need help, I’d be happy to give you my phone number. I will
            contact you as soon as possible
          </p>
          <form
            className='flex mx-3 lg:mx-32'
            onSubmit={formik.handleSubmit}>
            <div className='grid grid-cols-6 w-full gap-7'>
              <div className='col-span-6'>
                <Input
                  formik={formik}
                  name='name'
                  placeholder='Name company, personal'
                  type='text'
                />
              </div>
              <div className='col-span-6 md:col-span-3'>
                <Input
                  formik={formik}
                  name='email'
                  type='email'
                  placeholder='Email'
                />
              </div>
              <div className='col-span-6 md:col-span-3'>
                <Input
                  formik={formik}
                  name='phone'
                  placeholder='Mobile number'
                  type='text'
                />
              </div>
              <div className='col-span-6 mt-5 mx-auto'>
                <button
                  type='submit'
                  disabled={!formik.isValid}
                  className={
                    !formik.isValid
                      ? 'cursor-not-allowed bg-primary w-52 p-3 h-full lg:rounded-3xl rounded-2xl font-bold'
                      : 'cursor-pointer bg-primary w-52 p-3 h-full lg:rounded-3xl rounded-2xl font-bold'
                  }>
                  Send
                </button>
              </div>
            </div>
          </form>
        </div>
        <div className='lg:col-span-1 col-span-3 mt-5 lg:mt-0'>
          <Image
            src='/assets/images/star3.svg'
            alt='star3'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: '22px',
              height: 'auto',
            }}
            className='mx-auto'
          />
          <Image
            src='/assets/images/star1.svg'
            alt='star1'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: '60px',
              height: 'auto',
            }}
            className='ml-5'
          />
          <Image
            src='/assets/images/star4.svg'
            alt='star4'
            width='0'
            height='0'
            sizes='100vw'
            style={{
              width: '18px',
              height: 'auto',
            }}
            className='mx-auto'
          />
        </div>
      </div>
    </div>
  );
};

export default Form;
